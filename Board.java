public class Board{
    private Square[][] tictactoeBoard;
    
    //constructor
    public Board(){
        this.tictactoeBoard = new Square[3][3];
        for (int row = 0; row < tictactoeBoard.length; row++) {
            for (int col = 0; col < tictactoeBoard[row].length; col++) {
                this.tictactoeBoard[row][col] = Square.BLANK;
            }
        }
    }
    
    //toString() method to set look of board
    public String toString(){
        System.out.println("  0  1  2 ");
        for(int row = 0; row<this.tictactoeBoard.length; row++){
            System.out.print(row + " ");
            for(int col = 0; col<this.tictactoeBoard[row].length; col++){
            
               System.out.print(tictactoeBoard[row][col] + "  ");
                        
            }
            System.out.println();
        }
        
       return " ";     
    }
    
    //method that validates the token placed
    public boolean placeToken(int row, int col, Square playerToken) {
        boolean ouputValue;
        if(row < 0 || row >= tictactoeBoard.length || col < 0 || col > tictactoeBoard.length){
            return false;
        }
        else{
            if(this.tictactoeBoard[row][col] != Square.BLANK){
                return false;
            }
            else{
                tictactoeBoard[row][col] = playerToken;
                return true;
            }
        }
    }
    
    //checking if place is full or not
    public boolean checkIfFull(){
        for (int row = 0; row < tictactoeBoard.length; row++) {
            for (int col = 0; col < tictactoeBoard[row].length; col++) {
                if (tictactoeBoard[row][col] == Square.BLANK) {
                    return false; 
                }
            }
        }
        return true; 
    }
        
    //checking if there's a horizontal win
    private boolean checkIfWinningHorizontal(Square playerToken){
        for(int row = 0; row < tictactoeBoard.length; row++){
            if(tictactoeBoard[row][0] == playerToken && 
               tictactoeBoard[row][1] == playerToken && 
               tictactoeBoard[row][2] == playerToken) {
               return true; 
            }
            
        }
        return false;
    }
    
    //checking if there's a vertical win
    private boolean checkIfWinningVertical(Square playerToken){
         for(int col = 0; col < tictactoeBoard[0].length; col++){
            if(tictactoeBoard[0][col] == playerToken && 
               tictactoeBoard[1][col] == playerToken && 
               tictactoeBoard[2][col] == playerToken) {
               return true; 
            }
            
        }
        return false;
    }
    
    //checking if there's a diagonal win
    private boolean checkIfWinningDiagonal(Square playerToken){
            if((tictactoeBoard[0][0] == playerToken && 
               tictactoeBoard[1][1] == playerToken && 
               tictactoeBoard[2][2] == playerToken) || 
               (tictactoeBoard[0][2] == playerToken && 
               tictactoeBoard[1][1] == playerToken && 
               tictactoeBoard[2][0] == playerToken)) {
               return true; 
            }
        else{
            return false;
        }
    }
    
    //checks for a win
    public boolean checkIfWinning(Square playerToken){
        if(checkIfWinningHorizontal(playerToken) == true || 
           checkIfWinningVertical(playerToken) == true || 
           checkIfWinningDiagonal(playerToken) == true ){
            return true;
        }
        else{
            return false;
        }
    }
        
}