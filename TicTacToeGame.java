import java.util.Scanner;
public class TicTacToeGame{
    public static void main(String[]args){
        System.out.println("Welcome user!");
        
        int player1Wins = 0;
        int player2Wins = 0;

        boolean playAgain = true;

        while (playAgain == true) {
            Board game = new Board();
            boolean gameOver = false;
            int player = 1;
            Square playerToken = Square.X;
            int row;
            int col;
            Scanner scan = new Scanner(System.in);

            while(gameOver != true){
                System.out.println(game);
                if(player == 1){
                    playerToken = Square.X;
                }
                else{
                    playerToken = Square.O;
                }

                System.out.println("Player " + player + " it's your turn!");
                row = scan.nextInt();
                col = scan.nextInt();

                while(game.placeToken(row,col,playerToken) == false){
                    System.out.println("Invalid values, please enter new ones");
                    row = scan.nextInt();
                    col = scan.nextInt();
                }

                if(game.checkIfFull() == true){
                    System.out.println("It's a tie!");
                    gameOver = true;
                }
                else{
                    if(game.checkIfWinning(playerToken) == true){
                        System.out.println("Player " + player + " is the winner!");
                        if(player == 1){
                                player1Wins++;
                            } else {
                                player2Wins++;
                            }
                        gameOver = true;
                    }
                    else{
                        player++;
                        if(player > 2){
                            player = 1;
                        }
                    }
                }  
            }

            System.out.println("Player 1 wins: " + player1Wins);
            System.out.println("Player 2 wins: " + player2Wins);
            System.out.println("Do you want to play again? (Y/N)");
            String response = scan.next();
            if (response.equals("N") || response.equals ("n")) {
                playAgain = false;
            }

        }
    }
    
}